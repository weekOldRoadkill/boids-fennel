;; Configuration Function
(fn love.conf [t]
  (set t.window.resizable true)
  (set t.window.highdpi true)
  (set t.window.title :Boids)
  (set t.gammacorrect true)
  (set t.window.vsync 0)
  (set t.version :11.3)
  (set t.window.msaa 8))
