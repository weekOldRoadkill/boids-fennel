;; Imports
(local ffi (require :ffi))


;; Miscellaneous Functions
(fn square [a] (* a a))


;; FFI
(ffi.cdef "typedef struct {float x, y;} vec; typedef struct {vec pos, vel;} body;")
(local body (ffi.typeof :body))

(local vec (ffi.metatype :vec {:__add (fn [a b] (vec (+ a.x b.x) (+ a.y b.y)))
                               :__sub (fn [a b] (vec (- a.x b.x) (- a.y b.y)))
                               :__mul (fn [x a] (vec (* x a.x) (* x a.y)))
                               :__div (fn [a x] (vec (/ a.x x) (/ a.y x)))
                               :__mod (fn [a x] (vec (% a.x x) (% a.y x)))
                               :__unm (fn [a] (vec (- a.x) (- a.y)))
                               :__len (fn [a] (math.sqrt (+ (square a.x) (square a.y))))}))


;; Vector Forms
(fn direction-> [theta] (vec (math.cos theta) (math.sin theta)))
(fn direction [a] (math.atan2 a.y a.x))

(fn length* [a] (+ (square a.x) (square a.y)))
(fn distance* [a b] (length* (- a b)))
(fn distance [a b] (length (- a b)))

(macro scalar-> [expr] `(vec ,expr ,expr))

(fn normal [a] (/ a (length a)))


;; Game State Variables
(local boids (fcollect [_ 1 1000] (body (scalar-> (- (* 4 (love.math.random)) 2))
                                        (direction-> (* 2 math.pi (love.math.random))))))

(var (range separation-weight alignment-weight cohesion-weight) (values 0.2 1 4 200))
(var range-squared (square range))
(var follow-target? false)
(var mouse (scalar-> 0))

(var (show-range? show-vel?) (values true true))
(local camera (love.math.newTransform))
(var delta nil)


;; LÖVE's Resize Function
(fn love.resize [w h]
  (local scale (/ (math.min w h) 2))
  (local (neg-scale half-scale) (values (- scale) (/ scale 2)))
  (camera:setTransformation 0 0 0 half-scale half-scale (/ w neg-scale) (/ h neg-scale)))


;; LÖVE's Keyboard Function
(fn love.keypressed [key _ _]
  (match key
    :h (set show-range? (not show-range?))
    :j (set show-vel? (not show-vel?))
    "-" (set alignment-weight (/ alignment-weight 1.25))
    "=" (set alignment-weight (* alignment-weight 1.25))
    "[" (do (set range (/ range 1.25)) (set range-squared (square range)))
    "]" (do (set range (* range 1.25)) (set range-squared (square range)))
    :up (set separation-weight (* separation-weight 1.25))
    :down (set separation-weight (/ separation-weight 1.25))
    :left (set cohesion-weight (/ cohesion-weight 1.25))
    :right (set cohesion-weight (* cohesion-weight 1.25))))


;; LÖVE's Mouse Functions
(fn love.mousereleased [_ _ button _ _] (when (= button 1) (set follow-target? false)))
(fn love.mousepressed [_ _ button _ _] (when (= button 1) (set follow-target? true)))

(fn love.mousemoved [x y _ _ _]
  (local (x* y*) (camera:inverseTransformPoint x y))
  (set mouse (vec x* y*)))


;; LÖVE's Load Function
(fn love.load []
  (love.graphics.setLineWidth 0.003)
  (love.window.maximize))


;; LÖVE's Update Function
(fn love.update [delta*]
  (set delta delta*)
  (local boids* [(unpack boids)])
  (each [i boid (ipairs boids*)]
    (var (separation alignment cohesion close-boids) (values (scalar-> 0) (scalar-> 0) (scalar-> 0) 0))
    (each [j other-boid (ipairs boids*)]
      (local rel-pos (- boid.pos other-boid.pos))
      (local dist-squared (length* rel-pos))
      (when (and (< dist-squared range-squared) (not= i j))
        (set separation (+ separation (/ rel-pos (math.sqrt dist-squared))))
        (set alignment (+ alignment other-boid.vel))
        (set cohesion (- cohesion rel-pos))
        (set close-boids (+ close-boids 1))))
    (when (not= close-boids 0)
      (set alignment (/ alignment close-boids))
      (set cohesion (/ cohesion close-boids)))
    (local target (if follow-target? (normal (- mouse boid.pos)) (scalar-> 0)))
    (tset boids i :vel (normal (+ boid.vel (* delta (+ (* separation-weight separation)
                                                       (* alignment-weight alignment)
                                                       (* cohesion-weight cohesion)
                                                       (* 3 target))))))
    (tset boids i :pos (+ boid.pos (* delta boid.vel))))
  (each [_ boid (ipairs boids)]
    (when (< 2 (math.abs boid.pos.x))
      (set boid.pos.x (math.max -2 (math.min boid.pos.x 2)))
      (set boid.vel.x (- boid.vel.x)))
    (when (< 2 (math.abs boid.pos.y))
      (set boid.pos.y (math.max -2 (math.min boid.pos.y 2)))
      (set boid.vel.y (- boid.vel.y)))))


;; LÖVE's Draw Function
(fn love.draw []
  (love.graphics.applyTransform camera)
  (each [_ boid (ipairs boids)]
    (when show-range?
      (love.graphics.setColor 0 1 1 0.004)
      (love.graphics.circle :fill boid.pos.x boid.pos.y range)
      (love.graphics.setColor 1 1 1))
    (when show-vel?
      (local vel (+ (* -0.075 boid.vel) boid.pos))
      (love.graphics.line boid.pos.x boid.pos.y vel.x vel.y))
    (love.graphics.circle :fill boid.pos.x boid.pos.y 0.015))
  (love.graphics.origin)
  (love.graphics.print (string.format (table.concat ["Frame Rate: %d FPS"
                                                     "Range: %.2f"
                                                     "Separation Weight: %.2f"
                                                     "Alignment Weight: %.2f"
                                                     "Cohesion Weight: %d"]
                                                    "\n")
                                      (/ delta)
                                      range
                                      separation-weight
                                      alignment-weight
                                      cohesion-weight)))
