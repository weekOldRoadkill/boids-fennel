#!/bin/sh
mkdir -p build

compile_fennel() {
    fennel -c --require-as-include --use-bit-lib --lua luajit --no-compiler-sandbox "$1.fnl" >"build/$1.lua"
}
compile_fennel conf
compile_fennel main

cp -r license build

cd build
rm -r boids.love
zip -r9 boids.love *
